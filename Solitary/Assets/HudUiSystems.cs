﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HudUiSystems : MonoBehaviour
{
    public static HudUiSystems Instance = null;

    [SerializeField] RadialProgressBar mechFuel = null;
    [SerializeField] RadialProgressBar playerFood = null;
    [SerializeField] RadialProgressBar playerWater = null;
    [SerializeField] RadialProgressBar playerHeat = null;
    [SerializeField] RadialProgressBar playerSleep = null;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    // Update is called once per frame
    public void UpdateSystem()
    {
        UpdateMechFuelBar();
    }

    void UpdateMechFuelBar()
    {
        mechFuel.SetValue(GameplayManager.Instance.playerSurvivalSystem.MechFuel / 100f);

        playerFood.SetValue(GameplayManager.Instance.playerSurvivalSystem.PlayerFood / 100f);
        playerWater.SetValue(GameplayManager.Instance.playerSurvivalSystem.PlayerWater / 100f);
        playerHeat.SetValue(GameplayManager.Instance.playerSurvivalSystem.PlayerHeat / 100f);
        playerSleep.SetValue(GameplayManager.Instance.playerSurvivalSystem.PlayerSleep / 100f);
    }
}
