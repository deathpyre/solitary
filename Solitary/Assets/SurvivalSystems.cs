﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurvivalSystems : MonoBehaviour
{
    [SerializeField] float mechFuel = 0;

    [SerializeField] float playerFood = 0;
    [SerializeField] float playerWater = 0;
    [SerializeField] float playerHeat = 0;
    [SerializeField] float playerSleep = 0;

    [SerializeField] TemperatureZoneType currentPlayerZone = TemperatureZoneType.Mech;
    public TemperatureZone mechTemp;
    public TemperatureZone outsideTemp;
    public TemperatureZone insideTemp;

    // Start is called before the first frame update
    void Start()
    {
        mechTemp = new TemperatureZone(50, 50);
        outsideTemp = new TemperatureZone(100, 100);
        insideTemp = new TemperatureZone(70, 70);
    }

    // Update is called once per frame
    public  void UpdateSystem()
    {
        UpdateFuel();
        UpdatePlayer();
    }

    void UpdateFuel()
    {
        mechFuel -= 1 * Time.deltaTime;
    }

    void UpdatePlayer()
    {
        playerFood -= 1 * Time.deltaTime;
        playerWater -= 1 * Time.deltaTime;
        playerSleep -= 1 * Time.deltaTime;

        UpdateHeat();
    }

    public void GivePlayer(ItemType itemType, float itemValue)
    {
        switch (itemType)
        {
            case ItemType.Food:
                PlayerFood += itemValue;
                break;
            case ItemType.Water:
                PlayerWater += itemValue;
                break;
            case ItemType.Heat:
                PlayerHeat += itemValue;
                break;
            case ItemType.Sleep:
                PlayerSleep += itemValue;
                break;
            case ItemType.Fuel:
                MechFuel += itemValue;
                break;
        }
    }

    void UpdateHeat()
    {
        var targetTemp = 50f;
        switch (currentPlayerZone)//Grab the current environments temp
        {
            case TemperatureZoneType.Mech:
                targetTemp = mechTemp.currentTemp;
                break;
            case TemperatureZoneType.Outside:
                targetTemp = outsideTemp.currentTemp;
                break;
            case TemperatureZoneType.Inside:
                targetTemp = insideTemp.currentTemp;
                break;
            case TemperatureZoneType.Cave:
                break;
            case TemperatureZoneType.Shadowed:
                break;
        }

        if (targetTemp == playerHeat)//Only do temp change when not the same
            return;

        var difference = targetTemp - playerHeat;
        float rate = 0.2f;
        if (Mathf.Abs(difference) > 20)//Set scaler by how far away it is
            rate = 0.5f;
        else
        {
            rate = 0.2f;
        }

        playerHeat += rate * Time.deltaTime * (targetTemp > playerHeat ? 1 : -1);

        //Check if they are close enough
        difference = targetTemp - playerHeat;
        if (Mathf.Abs(difference) < 0.5)
            playerHeat = targetTemp;
    }

    public void SetPlayerTemperatureZone(TemperatureZoneType type)
    {
        currentPlayerZone = type;
    }


    public float MechFuel
    {
        get { return mechFuel; }
        set { mechFuel = value; }
    }

    public float PlayerFood
    {
        get { return playerFood; }
        set { playerFood = value; }
    }
    public float PlayerWater
    {
        get { return playerWater; }
        set { playerWater = value; }
    }
    public float PlayerHeat
    {
        get { return playerHeat; }
        set { playerHeat = value; }
    }
    public float PlayerSleep
    {
        get { return playerSleep; }
        set { playerSleep = value; }
    }    
}

public class TemperatureZone
{
    public TemperatureZone(float normally, float currently)
    {
        normalTemp = normally;
        currentTemp = currently;
    }
    public float normalTemp;
    public float currentTemp;
}

public enum TemperatureZoneType
{
    Mech,
    Outside,
    Inside,
    Cave,
    Shadowed
}
