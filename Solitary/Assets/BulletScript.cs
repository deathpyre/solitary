﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public int damage = 5;
    public float bulletDistance = 300;
    public float bulletLifeTime = 10;

    float usedLife = 0;
    Vector3 startPos = Vector3.zero;
    // Start is called before the first frame update
    void Start()
    {
        startPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        usedLife += Time.deltaTime;
        if(usedLife >= bulletLifeTime)
        {
            Destroy(gameObject);
        }
        if (Vector3.Distance(startPos, transform.position) >= bulletDistance)
            Destroy(gameObject);
    }

    private void OnCollisionEnter(Collision collision)
    {
        var character = collision.gameObject.GetComponent<CharacterBase>();
        if(character != null)
        {

        }

        Destroy(gameObject);
    }
}
