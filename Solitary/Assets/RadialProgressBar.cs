﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RadialProgressBar : MonoBehaviour
{
    [SerializeField] Image background = null;
    [SerializeField] Image mainBar = null;
    [SerializeField] Image center = null;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetValue(float value)
    {
        value = Mathf.Clamp(value, 0, 1);
        mainBar.fillAmount = value;
    }
}
