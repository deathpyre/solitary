﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiSmg : MonoBehaviour
{
    [SerializeField] Transform bulletPrefab = null;
    [SerializeField] float bulletSpeed = 10f;
    [SerializeField] float bps = 6f;
    [SerializeField] float burstLength = -1;//-1 is no burst
    [SerializeField] float burstOffLength = -1;//-1 is no burst
    [SerializeField] Transform bulletPosition = null;
    bool firing;
    float bulletInterval = 0;
    float burstTime = 0;
    bool burstOn = true;

    List<Transform> allBullets = new List<Transform>();
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!firing)
            return;

       burstTime += Time.deltaTime;
        if (!burstOn)
        {
            if(burstTime >= burstOffLength)
            {
                burstOn = true;
                burstTime -= burstOffLength;
            }
        }
        else
        {
            if (burstLength == -1 || burstTime <= burstLength)//While less than shooting time limit
            {
                bulletInterval += Time.deltaTime;
                //Debug.LogError(bulletInterval);
                var interval = 1f / bps;
                if (bulletInterval >= interval)
                {
                    //Debug.LogError("Fire");
                    bulletInterval -= interval;
                    var newBullet = GameObject.Instantiate(bulletPrefab, bulletPosition.position, bulletPosition.rotation, null) as Transform;
                    newBullet.GetComponent<Rigidbody>().velocity = bulletPosition.forward * bulletSpeed;
                }

                var bulletCount = allBullets.Count;
            }
            else
            {
                burstOn = false;
                burstTime -= burstLength;
            }
        }
    }

    public void FireAtPlayer()
    {
        firing = true;
    }

    public void StopFire()
    {
        firing = false;
    }
}
