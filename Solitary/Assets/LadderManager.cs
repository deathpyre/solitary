﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderManager : MonoBehaviour
{
    bool ladderEndsOutside;
    float ladderTimeTaken = 0;
    Vector3 endPosition = Vector3.zero;
    bool onLadder = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    bool waitingToDetach = false;
    // Update is called once per frame
    void Update()
    {
        if(waitingToDetach)
        {
            waitingToDetach = false;
            GameplayManager.Instance.LeaveCockpit();
        }
        if (!onLadder)
            return;

        ladderTimeTaken += Time.deltaTime;

        if (ladderTimeTaken > 3)
        {
            onLadder = false;
            ladderEndsOutside = false;
            GameplayManager.Instance.LadderOff();
            GameplayManager.Instance.TeleportPlayer(endPosition);
            waitingToDetach = true;
            //GameplayManager.Instance.LeaveCockpit();
        }
    }

    public void ClimbLadder(Vector3 localStartPosition,  Vector3 localEndPosition, bool endsOutside)
    {
        onLadder = true;
        ladderTimeTaken = 0;
        endPosition = localEndPosition;
        ladderEndsOutside = endsOutside;
    }
}
