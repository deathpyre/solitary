﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvancedPlayerController : MonoBehaviour
{
    public LayerMask exteriorLayerMask;
    public LayerMask interiorLayerMask;
    public static AdvancedPlayerController Instance = null;
    public UnityStandardAssets.Characters.FirstPerson.FirstPersonController unityController = null;
    public Camera mainCamera = null;
    public LayerMask interactionMask = new LayerMask();
    string ladderMask = "Ladder Collider";
    string pickupMask = "Interaction Collider";

    int interactionState = 0;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        unityController = GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();
        mainCamera = GetComponentInChildren<Camera>();
    }

    void Update()
    {
        RaycastHit hitInfo = new RaycastHit();
        if (Physics.Raycast(mainCamera.transform.position, mainCamera.transform.forward, out hitInfo, 2f, interactionMask))
        {
            interactionState = 1;
            if (hitInfo.collider.transform.gameObject.layer == LayerMask.NameToLayer(ladderMask))
            {
                GameplayManager.Instance.SetInteractionTextSimple("Climb Ladder");
                if (Input.GetKeyDown(KeyCode.F))
                {
                    hitInfo.collider.transform.GetComponent<LadderTrigger>().TriggerLadder(transform.position);
                }
            }
            else if (hitInfo.collider.transform.gameObject.layer == LayerMask.NameToLayer(pickupMask))
            {
                var item = hitInfo.collider.transform.GetComponent<ItemPickup>();
                GameplayManager.Instance.SetInteractionTextSimple(item.interactionText);
                if (Input.GetKeyDown(KeyCode.F))
                {
                    item.PickUp();
                }
            }
        }
        else
            interactionState = 0;




        if (interactionState == 0)
            GameplayManager.Instance.SetInteractionText("");
    }

    public void SetCameraToExterior()
    {
        GetComponentInChildren<Camera>().cullingMask = exteriorLayerMask;
        GetComponentInChildren<Camera>().clearFlags = CameraClearFlags.Skybox;
    }
    public void SetCameraToInterior()
    {
        GetComponentInChildren<Camera>().cullingMask = interiorLayerMask;
        GetComponentInChildren<Camera>().clearFlags = CameraClearFlags.Depth;
    }

    public void SetPlayerPhantom(bool phantom)
    {
        GetComponent<CharacterController>().enabled = !phantom;
        GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = !phantom;
    }
}
