﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemy : MonoBehaviour
{
    BreadcrumbAi.Ai aiScript = null;
    public float timeBeforeAttack;
    public float shootingLength;
    public float shootingHoldLength;

    float ableToAttackTime;
    float shootingTime;
    AiSmg gun = null;

    // Start is called before the first frame update
    void Start()
    {
        aiScript = GetComponent<BreadcrumbAi.Ai>();
        gun = GetComponentInChildren<AiSmg>();
    }

    // Update is called once per frame
    void Update()
    {
        //if(aiScript.moveState == BreadcrumbAi.Ai.MOVEMENT_STATE.IsFollowingPlayer)
        //{
            CheckAttacking();
        //}
    }

    void CheckAttacking()
    {
        switch(aiScript.attackState)
        {
            case BreadcrumbAi.Ai.ATTACK_STATE.CanAttackPlayer:
                ableToAttackTime += (1f / timeBeforeAttack) * Time.deltaTime;

                ableToAttackTime = Mathf.Clamp(ableToAttackTime, 0, 1);
                if (ableToAttackTime >= 1)//If we have waitied long enough aiming at player
                {
                    shootingTime += Time.deltaTime;
                    if (shootingTime < shootingLength)//While less than shooting time limit
                    {
                        gun.FireAtPlayer();
                    }
                    else
                    {
                        gun.StopFire();
                        if (shootingTime >= shootingLength + shootingHoldLength)//if cycle complete, reset
                            shootingTime -= shootingLength + shootingHoldLength;
                    }
                }
                break;
        }
    }
}
