﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMechWalker : MonoBehaviour
{
    public float metersPerSecond = 1;
    public Transform bounceSection = null;
    // Start is called before the first frame update
    void Start()
    {
    }

    float time = 0;
    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * (metersPerSecond * Time.deltaTime);

        time += Time.deltaTime*2;
        bounceSection.localPosition = new Vector3(0,Mathf.Sin(time)*0.2f, 0);
    }
}
