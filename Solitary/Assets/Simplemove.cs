﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Simplemove : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = AdvancedPlayerController.Instance.transform.localPosition;
        transform.localRotation = AdvancedPlayerController.Instance.transform.localRotation;
    }
}
