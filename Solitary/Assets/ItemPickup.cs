﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : MonoBehaviour
{
    public ItemType itemType = ItemType.Food;
    public float itemValue = 1;
    public string interactionText = "don't die";

    public void PickUp()
    {
        GameplayManager.Instance.GivePlayer(itemType, itemValue);
        Destroy(gameObject);
    }
}

public enum ItemType
{
    Fuel,
    Food,
    Water,
    Heat,
    Sleep
}
