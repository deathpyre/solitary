﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CockpitTrigger : MonoBehaviour
{
    public bool allowEnter = false;
    public bool allowLeave = false;
    void OnTriggerEnter(Collider other)
    {
        if (!allowEnter)
            return;

        if (other.tag == "Real Player")
        {
            Debug.LogError("Enter");
            GameplayManager.Instance.EnterCockpit();
        }
    }
    void OnTriggerExit(Collider other)
    {
        var name = transform.name;
        if (!allowLeave)
            return;

        if (other.tag == "Real Player")
        {
            Debug.LogError("Exit");
            GameplayManager.Instance.LeaveCockpit();
        }
    }
}
