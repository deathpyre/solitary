﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMirror : MonoBehaviour
{
    [SerializeField] Camera otherCamera = null;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Camera.main != null)
        {
            transform.localPosition = Camera.main.transform.localPosition;
            transform.localRotation = Camera.main.transform.localRotation;
        }
    }
}
