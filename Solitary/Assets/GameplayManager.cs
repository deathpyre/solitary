﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameplayManager : MonoBehaviour
{
    public static GameplayManager Instance = null;
    [SerializeField] LadderManager ladderManager = null;
    [SerializeField] Text interactionText = null;

    ExteriorMech exteriorMech = null;
    AdvancedPlayerController realPlayer = null;
    public Transform realMech = null;
    Transform fakePlayer = null;
    Transform exteriorRoot = null;
    Transform interiorMech = null;
    public bool onLadder = false;

    [Header("Systems")]
    public SurvivalSystems playerSurvivalSystem = null;

    bool inside = true;

    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
        exteriorMech = GameObject.FindObjectOfType<ExteriorMech>();
        realPlayer = GameObject.FindObjectOfType<AdvancedPlayerController>();
        realMech = GameObject.FindGameObjectWithTag("Real Mech").transform;
        fakePlayer = GameObject.FindGameObjectWithTag("Fake Player").transform;
        exteriorRoot = GameObject.FindGameObjectWithTag("Exterior Root").transform;
        interiorMech = GameObject.FindGameObjectWithTag("Interior Mech").transform;
    }

    // Update is called once per frame
    void Update()
    {
        playerSurvivalSystem.UpdateSystem();
        HudUiSystems.Instance.UpdateSystem();
    }

    public void Ladder(Vector3 localStartPosition, Vector3 localEndPosition, bool endsOutside)
    {
        if (!inside)
            EnterCockpit();

        Instance.TeleportPlayer(localStartPosition);

        onLadder = true;
        realPlayer.SetPlayerPhantom(true);
        ladderManager.ClimbLadder(localStartPosition, localEndPosition, endsOutside);
        return;

        if (inside)
            return;

        inside = true;
        exteriorMech.hideSection.SetActive(true);

        realPlayer.transform.SetParent(interiorMech);

        realPlayer.unityController.TeleportTo(localEndPosition, realPlayer.mainCamera.transform.rotation, true);

        realMech.SetParent(interiorMech);
        realMech.transform.localPosition = Vector3.zero;
        realMech.transform.localRotation = Quaternion.identity;
        realPlayer.SetCameraToInterior();
    }

    public void LadderOff()
    {
        onLadder = false;
        realPlayer.SetPlayerPhantom(false);

        
    }

    public void EnterCockpit()
    {
        if (inside || onLadder)
            return;

        inside = true;
        playerSurvivalSystem.SetPlayerTemperatureZone(TemperatureZoneType.Mech);

        exteriorMech.hideSection.SetActive(true);

        realPlayer.transform.SetParent(interiorMech);

        realPlayer.unityController.TeleportTo(exteriorMech.transform.InverseTransformPoint(realPlayer.transform.position), realPlayer.mainCamera.transform.rotation, true);

        realMech.SetParent(interiorMech);
        realMech.transform.localPosition = Vector3.zero;
        realMech.transform.localRotation = Quaternion.identity;
        realPlayer.SetCameraToInterior();
    }

    public void LeaveCockpit()
    {
        if (!inside || onLadder)
            return;

        inside = false;
        playerSurvivalSystem.SetPlayerTemperatureZone(TemperatureZoneType.Outside);

        realPlayer.transform.SetParent(exteriorRoot, true);
        realMech.transform.SetParent(exteriorMech.transform, true);
        realMech.transform.localPosition = Vector3.zero;
        realMech.transform.localRotation = Quaternion.identity;

        realPlayer.transform.rotation = (Quaternion)fakePlayer.rotation;
        realPlayer.unityController.TeleportTo(fakePlayer.position, fakePlayer.GetComponentInChildren<Camera>().transform.rotation);
        realPlayer.SetCameraToExterior();

        exteriorMech.hideSection.SetActive(false);
    }

    public void TeleportPlayer(Vector3 position, bool local = true)
    {
        realPlayer.unityController.TeleportTo(position, realPlayer.mainCamera.transform.rotation, local);
    }

    public void SetInteractionText(string text)
    {
        if (interactionText.text != text)
            interactionText.text = text;
    }

    public void SetInteractionTextSimple(string text)
    {
        if (interactionText.text != text)
            interactionText.text = "Press \"F\" to " + text;
    }

    public void GivePlayer(ItemType itemType, float itemValue)
    {
        playerSurvivalSystem.GivePlayer(itemType, itemValue);
    }
}
