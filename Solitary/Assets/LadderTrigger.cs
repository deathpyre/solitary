﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LadderTrigger : MonoBehaviour
{
    public Vector3 upEndPosition = Vector3.zero;
    public Vector3 downEndPosition = Vector3.zero;
    public void TriggerLadder(Vector3 position)
    {
        position = transform.InverseTransformPoint(position);
        var target = upEndPosition;
        var start = downEndPosition;
        if (Vector3.Distance(upEndPosition, position) < Vector3.Distance(downEndPosition, position))
        {
            target = downEndPosition;
            start = upEndPosition;
        }
        var localPos = GameplayManager.Instance.realMech.transform.InverseTransformPoint(transform.position);
        GameplayManager.Instance.Ladder(localPos + start, localPos + target, false);
    }
}
